import React, { Component } from 'react'
import Tratment_1 from './Tratment_1'
import { dataGlasses } from './Data_glass'

export default class Glass_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataGlasse: null
    }
  }

  render() {
    let imgSrc = "./glassesImage/model.jpg"
   
    return (
      <div className='container py-5' >
        {
          this.state.dataGlasse && (
            <div>

              <div>Name: {this.state.dataGlasse.name}</div>
              <div>Price: {this.state.dataGlasse.price}</div>
              <div>Description:  {this.state.dataGlasse.desc}</div>
            </div>
          )
        }
        <div className="col-4">
          <Tratment_1 url={this.state.dataGlasse ? this.state.dataGlasse.url: null} />
        </div>
        <div className="col-7">
          <div className="button-img col-12 d-flex">
            {
              dataGlasses.map((item, index) => (
                <button key={index} onClick={() => {
                  this.setState({
                    dataGlasse: item
                  })
                }} className="img-jpg">
                  <img className='w-100' src={item.url} alt="" />
                </button>
              ))
            }
          </div>
        </div>
      </div>
    )
  }
}
