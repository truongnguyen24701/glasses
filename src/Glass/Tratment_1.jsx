import React, { Component } from 'react'

export default class Tratment_1 extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let imgSrc = "./glassesImage/model.jpg"
        let imgSrc1 = "./glassesImage/v1.png"
        return (
            <div >
                <div className="card" style={{ width: '18rem' }}>
                    <img className='img1' src={imgSrc} alt="..." />
                    {
                        this.props.url &&
                        <img className='img2' src={this.props.url} alt="" />
                    }
                </div>
            </div>
        )
    }
}
